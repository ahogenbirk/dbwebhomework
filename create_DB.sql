Create table question(q_number int(2) NOT NULL AUTO_INCREMENT,
q_text varchar(100),
primary key(q_number))
engine=InnoDB;
Create table choice(c_number int(2) NOT NULL AUTO_INCREMENT,
c_test varchar (100),
correct varchar(3) CHECK(correct='yes' or correct='no'),
primary key(c_number))
engine=InnoDB;
Create table part_of(q_number int(2) NOT NULL AUTO_INCREMENT,
c_number int(2) NOT NULL AUTO_INCREMENT,
primary key(q_number,c_number),
foreign key(q_number) references question,
foreign key(c_number) references choice)
engine=InnoDB;
